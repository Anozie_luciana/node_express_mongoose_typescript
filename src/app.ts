import express from "express";
import dotenv from 'dotenv';
// import db from "mongoose";
import todoRoutes from './router/routerTodo';
import { json, urlencoded } from "body-parser";
import mongoose from "mongoose";

dotenv.config({path: "./.env"})
const port =process.env.PORT

const app = express();

app.use(json());

app.use(urlencoded({ extended: true }));

app.use("/todos", todoRoutes);

app.use(
  (
    err: Error,
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    res.status(500).json({ message: err.message });
  }
);

const url = process.env.DB

mongoose.set('strictQuery', false);
mongoose.connect(url).then(()=>{
  console.log("Database connected");
}).catch((e)=>{
  console.log(e.message);
})

// db.connect("mongodb://localhost:27017/todos", () => {
//   console.log("Database connected");
// });

app.listen(port, ()=>{
  console.log(`listening to server ${port}`)
})