import { Router } from "express";
import {
  createToDo,getToDo,updateToDo,deleteToDo, getOneTodo
} from "../controller/controllerTodo";

const router = Router();

router.post("/", createToDo);

router.get("/", getToDo);

router.patch("/:id", updateToDo);

router.get("/:id", getOneTodo);

router.delete("/:id", deleteToDo);

export default router